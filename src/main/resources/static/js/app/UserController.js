'use strict'

var module = angular.module('demo.controllers', []);
module.controller("UserController", [ "$scope", "UserService",
		function($scope, UserService) {

			$scope.userDto = {
				userId : null,
				userName : null,
				skillDtos : []
			};
			$scope.skills = [];
			
			UserService.getAllUsers().then(function(value) {
                $scope.allUsers= value.data;
            }, function(reason) {
                console.log("error occured");
            }, function(value) {
                console.log("no callback");
            });

			$scope.saveUser = function() {
				$scope.userDto.skillDtos = $scope.skills.map(skill => {
					return {skillId: null, skillName: skill};
				});
				UserService.saveUser($scope.userDto).then(function() {
					console.log("works");
					UserService.getAllUsers().then(function(value) {
						$scope.allUsers= value.data;
					}, function(reason) {
						console.log("error occured");
					}, function(value) {
						console.log("no callback");
					});

					$scope.skills = [];
					$scope.userDto = {
						userId : null,
						userName : null,
						skillDtos : []
					};
				}, function(reason) {
					console.log("error occured");
				}, function(value) {
					console.log("no callback");
				});
			}

			$scope.updateUser = function(user) {
                UserService.updateUser(user).then(function() {
                    console.log("works");
                    UserService.getAllUsers().then(function(value) {
                        $scope.allUsers= value.data;
                    }, function(reason) {
                        console.log("error occured");
                    }, function(value) {
                        console.log("no callback");
                    });

                    $scope.skills = [];
                    $scope.userDto = {
                        userId : null,
                        userName : null,
                        skillDtos : []
                    };
                }, function(reason) {
                    console.log("error occured");
                }, function(value) {
                    console.log("no callback");
                });
			}

			$scope.deleteUser = function(userId) {
            	UserService.deleteUser(userId).then(function() {
                    console.log("works");
                    UserService.getAllUsers().then(function(value) {
                        $scope.allUsers= value.data;
                    }, function(reason) {
                        console.log("error occured");
                    }, function(value) {
                        console.log("no callback");
                    });

                    $scope.skills = [];
                    $scope.userDto = {
                        userId : null,
                        userName : null,
                        skillDtos : []
                    };
                }, function(reason) {
                    console.log("error occured");
                }, function(value) {
                    console.log("no callback");
                });
            }
		} ]);
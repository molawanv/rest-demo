package com.example.demo.service;

import java.util.List;

import com.example.demo.dto.UserDto;

/**
 * Created by Vaibhav on 13/5/17.
 */
public interface UserService {
    UserDto getUserById(Integer userId);
    void saveUser(UserDto userDto);
    void updateUser(UserDto userDto);
    void deleteUser(int userId);
    List<UserDto> getAllUsers();
}
